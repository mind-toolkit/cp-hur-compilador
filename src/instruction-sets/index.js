import CPUv3 from './cp-hur-v3.json'
import CPUv4 from './cp-hur-v4.json'


export default {
	"CP-HUR v3": CPUv3,
	"CP-HUR v4": CPUv4
}
