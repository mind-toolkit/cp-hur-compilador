import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import InstructionSets from '../views/InstructionSets.vue'


const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/Instruction-Sets',
    name: 'Instruction Sets',
    component: InstructionSets
		,
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
